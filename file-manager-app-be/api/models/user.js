const mongoose = require("mongoose");

var Users = new mongoose.model(
    "users",
    new mongoose.Schema({
      _id: mongoose.Types.ObjectId,
      name: String,
      password: String
    })
);
  
module.exports = Users;