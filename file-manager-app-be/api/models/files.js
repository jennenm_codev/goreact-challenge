const mongoose = require("mongoose");

var UploadFiles = new mongoose.model(
    "files",
    new mongoose.Schema({
      filename: String,
      mediaType: String,
      size: String,
      title: String,
      description: String,
      userUploaderId: String
    })
);
  
module.exports = UploadFiles;