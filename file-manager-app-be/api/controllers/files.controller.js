const path = require('path');
const fs = require('fs');
const multer = require('multer');
const UploadFiles = require("../models/files");

// File storage and upload utilities
const UPLOAD_DIR = './uploads/';
const storage = multer.diskStorage({
    destination: function (req, file, cb) {
        const dir = path.join(UPLOAD_DIR, req.body.userId);
        if (!fs.existsSync(UPLOAD_DIR)) {
            fs.mkdirSync(UPLOAD_DIR);
        }
        if (!fs.existsSync(dir)){
            fs.mkdirSync(dir);
        }
        cb(null, dir);
    },
    filename: function (req, file, cb) {
        cb(null, file.originalname)
    }
 });
const upload = multer({storage}).single('file');

exports.doUploadFile = (req, res) => {
    upload(req, res, function (err) {
       if (err) {
            console.log('upload error - ', err)
            return res.status(422).send({message: "An error occured."})
       } else {
            UploadFiles.findOne({
                filename: req.file.filename
            })
            .exec((fileErr, file) => {
                if (fileErr) {
                    res.status(500).send({ message: fileErr });
                    return;
                }

                if (!file) {
                    const fileSchema = new UploadFiles({
                        filename: req.file.filename,
                        mediaType: req.file.mimetype,
                        size: req.file.size,
                        title: req.body.title,
                        description: req.body.description,
                        userUploaderId: req.body.userId
                    })

                    fileSchema.save(function(saveErr, uploadFile) {
                        if(saveErr) {
                            console.log('save error - ', saveErr);
                            return res.status(500).send({message: "An error occured."})
                        } else {
                            return res.send({file: uploadFile}); 
                        }
                    });
                } else {
                    try {
                        fs.unlinkSync(req.file.path);
                    } catch(unlinkErr) {
                        console.error(unlinkErr)
                    }
                }
            });
        }
    });
};

exports.getAllUploads = (req, res) => {
    UploadFiles.find({
        userUploaderId: req.query.userId
    })
    .exec((err, files) => {
        return res.send({files}); 
    });
}

exports.deleteFile = (req, res) => {
    UploadFiles.deleteOne({
        _id: req.body.fileId
    })
    .exec((err, files) => {
        try {
            let filePath = path.join(UPLOAD_DIR, req.body.userUploaderId);
            filePath = path.join(filePath, req.body.filename);
            fs.unlinkSync(filePath);
        } catch(unlinkErr) {
            console.error(unlinkErr)
        }

        return res.send({message: 'Deleted.'}); 
    });
}

exports.getFile = (req, res) => {
    let filePath = path.join(UPLOAD_DIR, req.query.userUploaderId);
    filePath = path.join(filePath, req.query.filename);
    res.sendFile(path.resolve(filePath));
}