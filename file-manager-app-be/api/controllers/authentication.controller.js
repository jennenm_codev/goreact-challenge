const Users = require("../models/user");

exports.doLogin = (req, res) => {
  Users.findOne({
    username: req.body.username
  })
  .exec((err, user) => {
    if (err) {
        res.status(500).send({ message: err });
        return;
    }

    if (!user) {
        return res.status(404).send({ message: "User not found." });
    }

    var passwordIsValid = (req.body.password == user.password);

    if (!passwordIsValid) {
        return res.status(401).send({
          accessToken: null,
          message: "Invalid password."
        });
    }

    res.status(200).send({
        _id: user._id,
        name: user.name,
        username: user.username
    });
  });
};