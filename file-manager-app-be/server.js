let express = require('express'),
   path = require('path'),
   mongoose = require('mongoose'),
   cors = require('cors'),
   bodyParser = require('body-parser');
const http = require("http");

// Connecting with mongo db
mongoose.Promise = global.Promise;
mongoose.connect("mongodb+srv://admin:1@cluster0.gdkfp.mongodb.net/goreact-file-manager?retryWrites=true&w=majority", {
   useNewUrlParser: true,
   useUnifiedTopology: true
}).then(() => {
      console.log('Database sucessfully connected')
   },
   error => {
      console.log('Database could not connected: ' + error)
   }
)

// Setting up express js
const app = express();
app.use((req, res, next) => {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
    res.header('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, OPTIONS');
    next();
});
app.use(bodyParser.urlencoded({
   extended: true
}));
app.use(bodyParser.json());

// APIs
const authAPI = require('./api/controllers/authentication.controller')
app.post('/api/login', async(req,res) => {
    authAPI.doLogin(req, res);
});

const filesAPI = require('./api/controllers/files.controller')
app.post('/api/files/upload', function (req, res, next) {
    filesAPI.doUploadFile(req, res);
})

app.get('/api/files/all', function (req, res, next) {
    filesAPI.getAllUploads(req, res);
})

app.post('/api/files/delete', function (req, res, next) {
    filesAPI.deleteFile(req, res);
})

app.get('/api/files/preview', function (req, res, next) {
    filesAPI.getFile(req, res);
})

// Error handler
app.use(function (err, req, res, next) {
  if (!err.statusCode) err.statusCode = 500;
  res.status(err.statusCode).send(err.message);
});

// Listen port
http.createServer(app).listen(4000, ()=> {
    console.log(`Server is running on 4000`);
});