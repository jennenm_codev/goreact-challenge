import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

// Constants
import { RoutePath } from './core/constants/Routes';

// Components
import { LoginComponent } from './features/authentication/components/login/login.component';
import { LayoutComponent } from './core/components/layout/layout.component';

const routes: Routes = [
  {
    path: RoutePath.Login,
    component: LoginComponent
  },
  {
    path: RoutePath.Files.Root,
    component: LayoutComponent,
    loadChildren: () => import('./features/file-manager/file-manager.module').then(module => module.FileManagerModule)
  },
  {
    path: '',
    redirectTo: RoutePath.Login,
    pathMatch: 'full'
  },
  {
    path: '**',
    redirectTo: RoutePath.Login,
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
