import { Injectable } from '@angular/core';
import { Resolve } from '@angular/router';
import { Observable } from 'rxjs';

// Services
import { FileUploadService } from '../services/file-upload.service';

@Injectable({
  providedIn: 'root'
})
export class FilesListResolverService implements Resolve<any> {

  constructor(private fileUploadService: FileUploadService) { }

  resolve(): Observable<any[]> {
    return this.fileUploadService.getAllFiles();
  }
}
