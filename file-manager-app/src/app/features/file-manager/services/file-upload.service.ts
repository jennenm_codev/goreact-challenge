import { Injectable } from '@angular/core';

// Constants
import { API } from 'src/app/core/constants/API';

// Services
import { RequestService } from 'src/app/core/services/request/request.service';

@Injectable({
  providedIn: 'root'
})
export class FileUploadService {

  constructor(private request: RequestService) { }

  doUpload(fileData: any) {
    return this.request.upload(API.files.root + API.files.upload, fileData);
  }

  getAllFiles() {
    const user = JSON.parse(sessionStorage.getItem('sessionUser'));
    return this.request.get(API.files.root + API.files.all, {userId: user._id});
  }

  deleteFile(fileData: any) {
    return this.request.post(API.files.root + API.files.delete, fileData);
  }

  getFile(fileData: any) {
    return this.request.get(API.files.root + API.files.preview, fileData);
  }
}
