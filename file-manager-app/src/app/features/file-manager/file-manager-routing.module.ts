import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

// Components
import { FileListComponent } from './components/file-list/file-list.component';

// Services
import { FilesListResolverService } from './resolvers/files-list-resolver.service';

const routes: Routes = [
  {
    path: '',
    component: FileListComponent,
    resolve: {
      allFiles: FilesListResolverService
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FileManagerRoutingModule { }
