import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef } from '@angular/material/dialog';

// Services
import { FileUploadService } from '../../services/file-upload.service';

@Component({
  selector: 'app-file-upload-dialog',
  templateUrl: './file-upload-dialog.component.html',
  styleUrls: ['./file-upload-dialog.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class FileUploadDialogComponent implements OnInit {
  FileUploadForm: FormGroup;
  selectedFileName = '';
  selectedFileBuffer: any;

  constructor(
    public dialogRef: MatDialogRef<FileUploadDialogComponent>,
    private fb: FormBuilder,
    private fileUploadService: FileUploadService
  ) { }

  ngOnInit() {
    this.FileUploadForm = this.fb.group({
      Filename: [{value: '', disabled: true}, Validators.required],
      Type: [{value: '', disabled: true}],
      FileData: [],
      Title: ['', Validators.required],
      Description: ['', Validators.required]
    });
  }

  onFileSelected(event) {
    this.FileUploadForm.get('Filename').setValue(event.target.files[0].name);
    this.FileUploadForm.get('Type').setValue(event.target.files[0].type);
    this.FileUploadForm.get('FileData').setValue(event.target.files[0]);
  }

  doUpload() {
    this.FileUploadForm.updateValueAndValidity();

    const fileFormData = new FormData();

    const user = JSON.parse(sessionStorage.getItem('sessionUser'));
    const filename = this.FileUploadForm.get('Filename').value;
    const fileData = this.FileUploadForm.get('FileData').value;
    const type = this.FileUploadForm.get('Type').value;
    const title = this.FileUploadForm.get('Title').value;
    const description = this.FileUploadForm.get('Description').value;

    fileFormData.append('userId', user._id);
    fileFormData.append('filename', filename);
    fileFormData.append('type', type);
    fileFormData.append('file', fileData);
    fileFormData.append('title', title);
    fileFormData.append('description', description);

    this.fileUploadService.doUpload(fileFormData).subscribe(
      result => {
        this.dialogRef.close();
      }
    );
  }
}
