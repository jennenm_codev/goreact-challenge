import { Component, EventEmitter, Input, OnInit, Output, ViewEncapsulation } from '@angular/core';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { DomSanitizer } from '@angular/platform-browser';

// Constants
import { API } from 'src/app/core/constants/API';

// Services
import { FileUploadService } from '../../services/file-upload.service';

// Components
import { PreviewDialogComponent } from '../preview-dialog/preview-dialog.component';

@Component({
  selector: 'app-gallery',
  templateUrl: './gallery.component.html',
  styleUrls: ['./gallery.component.css']
})
export class GalleryComponent implements OnInit {

  @Input() list: any;
  // tslint:disable-next-line: no-output-on-prefix
  @Output() onDelete: EventEmitter<any>;

  constructor(
    public dialog: MatDialog,
    private sanitizer: DomSanitizer,
    private fileUploadService: FileUploadService
  ) {
    this.onDelete = new EventEmitter<any>();
  }

  ngOnInit() {
  }

  deleteFile(file: any) {
    const user = JSON.parse(sessionStorage.getItem('sessionUser'));
    const fileData = {
      fileId: file._id,
      userUploaderId: user._id,
      filename: file.filename
    };

    this.fileUploadService.deleteFile(fileData).subscribe(
      result => {
        this.onDelete.emit(true);
        console.log('delete - ', result);
      }
    );
  }

  previewFile(file: any) {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.autoFocus = true;
    dialogConfig.closeOnNavigation = true;
    dialogConfig.disableClose = false;
    dialogConfig.hasBackdrop = true;

    const user = JSON.parse(sessionStorage.getItem('sessionUser'));
    dialogConfig.data = {
      userUploaderId: user._id,
      filename: file.filename,
      mediaType: file.mediaType
    };

    const dialogRef = this.dialog.open(PreviewDialogComponent, dialogConfig);
    dialogRef.afterClosed().subscribe(isOK => {
      // do nothing
    });
  }

  getUrl(file) {
    const user = JSON.parse(sessionStorage.getItem('sessionUser'));
    let completeURL = API.server + API.files.root + API.files.preview + '?';
    completeURL += 'userUploaderId=' + user._id + '&';
    completeURL += 'filename=' + file.filename;

    return this.sanitizer.bypassSecurityTrustResourceUrl(completeURL);
  }
}
