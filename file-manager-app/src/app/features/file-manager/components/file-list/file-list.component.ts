import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ActivatedRoute } from '@angular/router';
import { distinctUntilChanged } from 'rxjs/operators';

// Services
import { FileUploadService } from '../../services/file-upload.service';

// Components
import { FileUploadDialogComponent } from '../file-upload-dialog/file-upload-dialog.component';

@Component({
  selector: 'app-file-list',
  templateUrl: './file-list.component.html',
  styleUrls: ['./file-list.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class FileListComponent implements OnInit {
  public allFiles: any[];
  public allImages: any[];
  public allVideos: any[];
  public selectedTab: any;
  public SearchForm: FormGroup;

  constructor(
    private fb: FormBuilder,
    public dialog: MatDialog,
    private snackbar: MatSnackBar,
    private activatedRoute: ActivatedRoute,
    private fileUploadService: FileUploadService
  ) { }

  ngOnInit() {
    this.SearchForm = this.fb.group({
      SearchKey: ['']
    });

    this.selectedTab = 0;

    this.activatedRoute.data.subscribe(data => {
      if (data.allFiles != null) {
        this.allFiles = data.allFiles.files;
        this.setFilteredLists();
      }
    });

    this.SearchForm.get('SearchKey').valueChanges
    .pipe( distinctUntilChanged() )
    .subscribe(form => {
      console.log('form - ', form);
      if (form === '') {
        this.refreshList();
      }
    });
  }

  refreshList() {
    this.fileUploadService.getAllFiles().subscribe(
      result => {
        this.allFiles = result.files;
        this.setFilteredLists();
      },
      error => {
        this.snackbar.open('System error.', 'Close', {
          duration: 2000,
        });
      }
    );
  }

  setFilteredLists() {
    if (this.allFiles) {
      this.allImages = this.allFiles.filter((file) => {
        return file.mediaType.startsWith('image');
      });

      this.allVideos = this.allFiles.filter((file) => {
        return file.mediaType.startsWith('video');
      });
    }
  }

  doFilteredSearch() {
    if (this.allFiles) {
      const searchKey = this.SearchForm.get('SearchKey').value;
      this.allImages = this.allFiles.filter((file) => {
        if (file.mediaType.startsWith('image')) {
          if (file.title.includes(searchKey) || file.description.includes(searchKey)) {
            return file;
          }
        }
      });

      this.allVideos = this.allFiles.filter((file) => {
        if (file.mediaType.startsWith('video')) {
          if (file.title.includes(searchKey) || file.description.includes(searchKey)) {
            return file;
          }
        }
      });
    }
  }

  openSelectFileDialog() {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.autoFocus = true;
    dialogConfig.closeOnNavigation = false;
    dialogConfig.disableClose = true;
    dialogConfig.hasBackdrop = true;

    const dialogRef = this.dialog.open(FileUploadDialogComponent, dialogConfig);
    dialogRef.afterClosed().subscribe(isOK => {
      this.refreshList();
    });
  }

  changeTab(event) {
    // console.log('changeTab - select');
  }

  onFileDelete(event) {
    this.refreshList();
  }

  onKeydown(event) {
    if (event.keyCode === 13) {
      const searchKey = this.SearchForm.get('SearchKey').value;
      if (searchKey === '') {
        this.refreshList();
      } else {
        this.doFilteredSearch();
      }
    }
  }
}
