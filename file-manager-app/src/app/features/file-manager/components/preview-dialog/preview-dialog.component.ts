import { Component, ElementRef, Inject, OnInit, ViewChild } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';

// Constants
import { API } from 'src/app/core/constants/API';

// Services
import { FileUploadService } from '../../services/file-upload.service';

@Component({
  selector: 'app-preview-dialog',
  templateUrl: './preview-dialog.component.html',
  styleUrls: ['./preview-dialog.component.css']
})
export class PreviewDialogComponent implements OnInit {
  @ViewChild('videoPlayer') videoplayer: ElementRef;
  file: any;
  url: SafeResourceUrl;

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    private sanitizer: DomSanitizer,
    private fileUploadService: FileUploadService
  ) { }

  ngOnInit() {
    let completeURL = API.server + API.files.root + API.files.preview + '?';
    completeURL += 'userUploaderId=' + this.data.userUploaderId + '&';
    completeURL += 'filename=' + this.data.filename;

    this.url = this.sanitizer.bypassSecurityTrustResourceUrl(completeURL);
  }


  toggleVideo(event) {
    this.videoplayer.nativeElement.play();
  }
}
