import { Injectable } from '@angular/core';

// Services
import { RequestService } from 'src/app/core/services/request/request.service';

// Models/Constants
import { API } from 'src/app/core/constants/API';
import { CredentialsSchema } from '../models/Credentials';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {

  constructor(private request: RequestService) { }

  doLogin(credentials: CredentialsSchema) {
    return this.request.post(API.auth.login, credentials);
  }
}
