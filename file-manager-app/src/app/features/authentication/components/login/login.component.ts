import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { MatSnackBar } from '@angular/material/snack-bar';

// Constants
import { RoutePath } from 'src/app/core/constants/Routes';
import { CredentialsSchema } from '../../models/Credentials';

// Services
import { AuthenticationService } from '../../services/authentication.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  public LoginForm: FormGroup;

  constructor(
    private fb: FormBuilder,
    private router: Router,
    private authService: AuthenticationService,
    private snackbar: MatSnackBar
  ) { }

  ngOnInit() {
    this.LoginForm = this.fb.group({
      Username: ['', Validators.required],
      Password: ['', Validators.required]
    });
  }

  doLogin() {
    const creds = new CredentialsSchema();
    creds.username = this.LoginForm.get('Username').value;
    creds.password = this.LoginForm.get('Password').value;

    this.authService.doLogin(creds).subscribe(
      result => {
        this.router.navigate([RoutePath.Files.Root]);
        sessionStorage.setItem('sessionUser', JSON.stringify(result));
      },
      error => {
        let message = '';
        if (error.status === 404) {
          message = 'Incorrect username and/or password.';
        } else if (error.status === 500) {
          message = 'System error.';
        }

        this.snackbar.open(message, 'Close', {
          duration: 2000,
        });
      }
    );
  }

  onKeydown(event) {
    if (event.keyCode === 13) {
      this.doLogin();
    }
  }
}
