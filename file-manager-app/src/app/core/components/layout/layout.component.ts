import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { Router } from '@angular/router';

// Constants
import { RoutePath } from '../../constants/Routes';

@Component({
  selector: 'app-layout',
  templateUrl: './layout.component.html',
  styleUrls: ['./layout.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class LayoutComponent implements OnInit {
  public userName: string;

  constructor(private router: Router) { }

  ngOnInit() {
    const user = JSON.parse(sessionStorage.getItem('sessionUser'));
    this.userName = user.name;
  }

  doLogout() {
    sessionStorage.removeItem('sessionUser');
    this.router.navigate([RoutePath.Login]);
  }
}
