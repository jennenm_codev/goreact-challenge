import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

// Constants
import { API } from '../../constants/API';

@Injectable({
  providedIn: 'root'
})
export class RequestService {
  private httpOptionsJson = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  };

  constructor(
    private http: HttpClient
  ) { }

  get(url: string, ...params): Observable<any> {
    let completeURL = API.server + url;

    if (params.length !== 0) {
      for (const paramObj of params) {
        completeURL += '?';
        const paramKeys = Object.keys(paramObj);
        for (let i = 0; i < paramKeys.length; i++) {
          if (i === 0) {
            completeURL += '';
          } else {
            completeURL += '&';
          }

          completeURL += paramKeys[i] + '=' + paramObj[ paramKeys[i] ];
        }
      }
    }

    return this.http.get<any>(completeURL);
  }

  post(url: string, body: any): Observable<any> {
    const completeURL = API.server + url;
    return this.http.post<any>(completeURL, body, this.httpOptionsJson);
  }

  postWithParams(url: string, body: any, ...params): Observable<any> {
    let completeURL = API.server + url;

    if (params.length !== 0) {
      for (const paramObj of params) {
        completeURL += '?';
        const paramKeys = Object.keys(paramObj);
        for (let i = 0; i < paramKeys.length; i++) {
          if (i === 0) {
            completeURL += '';
          } else {
            completeURL += '&';
          }

          completeURL += paramKeys[i] + '=' + paramObj[ paramKeys[i] ];
        }
      }
    }
    return this.http.post<any>(completeURL, body, this.httpOptionsJson);
  }

  put(url: string, body: any): Observable<any> {
    const completeURL = API.server + url;
    return this.http.put<any>(completeURL, body, this.httpOptionsJson);
  }

  upload(url: string, body: any): Observable<any> {
    const completeURL = API.server + url;
    return this.http.post<any>(completeURL, body);
  }

  uploadWithParams(url: string, body: any, ...params): Observable<any> {
    let completeURL = API.server + url;

    if (params.length !== 0) {
      for (const paramObj of params) {
        completeURL += '?';
        const paramKeys = Object.keys(paramObj);
        for (let i = 0; i < paramKeys.length; i++) {
          if (i === 0) {
            completeURL += '';
          } else {
            completeURL += '&';
          }

          completeURL += paramKeys[i] + '=' + paramObj[ paramKeys[i] ];
        }
      }
    }
    return this.http.post<any>(completeURL, body);
  }
}
