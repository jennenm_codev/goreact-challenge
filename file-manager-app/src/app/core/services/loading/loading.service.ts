import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class LoadingService {
  public status: Subject<any> = new Subject();

  start() {
    this.status.next(true);
  }

  stop() {
    this.status.next(false);
  }
}
