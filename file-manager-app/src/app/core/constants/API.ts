export const API = {
  server: 'http://localhost:4000/api',
  auth: {
    login: '/login'
  },
  files: {
    root: '/files',
    upload: '/upload',
    all: '/all',
    delete: '/delete',
    preview: '/preview'
  }
};
