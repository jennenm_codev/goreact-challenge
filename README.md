# JFile Manager
Jen's submission for GoReact's code challenge.

Project was developed using MEAN stack.


## Prerequisites

Install **Node.js**. For more information, see [nodejs.org](https://nodejs.org/en/).

Install **Angular CLI**. For more information, see [angular.io](https://angular.io/guide/setup-local).


## Running the Backend

Next, we'll run the backend project.

1. In your favorite code editor, load the **file-manager-app-be** project in this directory.
2. Open the terminal and type in the command
```bash
npm install
```
3. Afterwhich, again in the terminal, type in the command
```bash
npm start
```
4. You should see this in your terminal
```bash
Server is running on 4000
Database sucessfully connected
```

## Running the Frontend

Next, we'll run the frontend project.

1. In a separate editor window, load the **file-manager-app** project in this directory.
2. Similar with the backend, open the terminal and type in the command
```bash
npm install
```
3. Then type in the command
```bash
npm start
```
4. You should see this in your terminal
```bash
Angular Live Development Server is listening on localhost:4200, open your browser on http://localhost:4200/
Compiled successfully.
```

## Exploring the application
Finally, we will run JFile Manager.
1. Launch **http://localhost:4200/** in your favorite browser.
2. There are only **two (2) test users** in the database
```bash
username: tester1
password: tester1
```
```bash
username: tester2
password: tester2
```
3. Login using any of the test user accounts.
4. Explore the app.

---